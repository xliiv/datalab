import io
import os
import tempfile

import pytest
from flask import url_for
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

# FIXME: https://flask.palletsprojects.com/en/1.1.x/config/#development-production
os.environ["DATALAB_UPLOAD_FOLDER"] = tempfile.mkdtemp()
os.environ["DATALAB_SQLALCHEMY_DATABASE_URI"] = 'sqlite:///:memory:'

from datalab import __version__
from datalab import models
from datalab.app import app, migrate


FILE_DATA = b"""\
State	Town	7_2009	3	4	8.40188
Alabama	Abbeville 	2930	3	10	3.94383"""


@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            models.db.create_all()

        yield client


def test_version():
    assert __version__ == '0.1.0'


def test_dataset_can_be_uploaded(client):
    """Test can upload data set."""
    filename = 'dataset'
    data = {
        'dataset': (io.BytesIO(FILE_DATA), filename),
    }

    response = client.post(
        'dataset/add',
        data=data,
        follow_redirects=True,
        content_type='multipart/form-data',
    )

    assert b'Your Dataset is saved' in response.data
    found = models.Dataset.query.get(1)
    assert found.name != filename
    assert found.original_name == filename
    assert [c.name for c in found.cols] == [
        'State', 'Town', '7_2009', '3', '4', '8.40188'
    ]
    with open(found.filepath) as f:
        assert len(f.read()) == len(FILE_DATA)
