import os
import shutil
import uuid

import pandas
from sqlalchemy.orm import relationship
from werkzeug.utils import secure_filename

from datalab.app import db


class Dataset(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    original_name = db.Column(db.String(128))
    cols = relationship("DatasetColumn")

    @property
    def filepath(self):
        from datalab.app import UPLOAD_FOLDER
        return os.path.join(UPLOAD_FOLDER, self.name)

    @classmethod
    def from_file(cls, src_file, dst_dir):
        """Creates DataSet from src_file object."""
        df = pandas.read_table(src_file)
        src_file.seek(0)
        cols = df.columns.values.tolist()
        cols = [DatasetColumn(name=col) for col in cols]
        name = str(uuid.uuid1())
        dst = os.path.join(dst_dir, name)
        with open(dst, 'wb') as dst_file:
            shutil.copyfileobj(src_file, dst_file)
        original_name = secure_filename(src_file.filename)
        instance = cls(name=name, original_name=original_name, cols=cols)
        return instance


class DatasetColumn(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    dataset_id = db.Column(db.Integer, db.ForeignKey('dataset.id'))
