import os

import pandas as pd
from flask import (
    Flask,
    flash, request,
    redirect,
    render_template,
    url_for,
)
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename


app = Flask(__name__)
app.secret_key = os.getenv('DATALAB_SECRET_KEY', str(os.urandom(16)))

# static dir for plots
PLOT_DIR = os.getenv('DATALAB_PLOT_DIR', './static/benford_plots')
os.makedirs(PLOT_DIR, exist_ok=True)

# uploads
UPLOAD_FOLDER = os.getenv('DATALAB_UPLOAD_FOLDER', 'uploads')
os.makedirs(UPLOAD_FOLDER, exist_ok=True)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# db migrations
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv(
    'DATALAB_SQLALCHEMY_DATABASE_URI', 'sqlite:///app.db'
)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# should be after app configuration
from . import models
from . import tools


@app.route('/')
def home():
    return render_template("base.html")


@app.route('/dataset/add', methods=['GET', 'POST'])
def dataset_add():
    """Uploads a new dataset."""
    if request.method == "POST":
        if 'dataset' not in request.files:
            # guard request has the file part
            flash('No file part')
            return redirect(request.url)
        file = request.files['dataset']
        if file.filename == '':
            # guard submit is not empty
            flash('No selected file')
            return redirect(request.url)

        if file:
            try:
                dataset = models.Dataset.from_file(
                    file, app.config['UPLOAD_FOLDER'],
                )
            except pd.errors.ParserError:
                flash('Ensure your file is correctly formatted tab-delimeted file.')  # noqa
                return redirect(request.url)
            db.session.add(dataset)
            db.session.commit()
            flash('Your Dataset is saved')
            return redirect(url_for('dataset_list'))
    return render_template("dataset_add.html")


@app.route('/dataset/list')
def dataset_list():
    datasets = models.Dataset.query.all()
    return render_template("dataset_list.html", datasets=datasets)


@app.route('/dataset/<dataset_id>/benford/<col_name>')
def dataset_benford(dataset_id, col_name):
    dataset = models.Dataset.query.get_or_404(dataset_id)
    plot_url = url_for('static', filename=f'benford_plots/{dataset.name}.png')

    if col_name not in {c.name for c in dataset.cols}:
        flash('Picked column is not from the dataset.')
        return redirect(url_for('dataset_list'))

    plot_path = f'.{plot_url}'
    try:
        data = pd.read_table(dataset.filepath)
        data = data.query(f"`{col_name}` > 1")
        left_digit_count = data[col_name] \
            .apply(lambda x: int(str(x)[:1])) \
            .value_counts()
    except (TypeError, ValueError, IndexError):
        flash(f"Benford's Law can't be checked for column '{col_name}'")
        return redirect(url_for('dataset_list'))
    try:
        benford_met = tools.benford_met(left_digit_count)
        tools.plot_to_benford(left_digit_count, 'Left Digit Counts', plot_path)
    except Exception:
        flash("Benford's Law couldn't be checked")
        return redirect(url_for('dataset_list'))
    return render_template(
        "dataset_benford.html",
        plot_url=plot_url,
        col_name=col_name,
        benford_met=benford_met,
    )
