import matplotlib.pyplot as plt
import numpy as np


def gen_benford_values(series):
    """Generates Benford's law values for `series`."""
    total = series.sum()
    sorted_series = series.sort_index()
    index_sorted = sorted_series.index.array.to_numpy()
    return np.log10(1 + 1./index_sorted) * total


def plot_to_benford(counts, title, dst_path):
    """Saves plot of `counts` compared to Benford's law.

    counts: A pandas series of counts with digits as index.
    title: The plot's title.
    """
    sorted_counts = counts.sort_index()
    plt.plot(
        sorted_counts.index.array.to_numpy(),
        gen_benford_values(sorted_counts),
        label="Benford\'s Law",
    )
    plt.bar(sorted_counts.index.values - 0.4, counts, label="Actual Counts")
    plt.title(title)
    plt.legend()
    plt.savefig(dst_path)


def benford_met(series):
    """Checks if Benford's law is met for `series`."""
    benford_values = gen_benford_values(series)
    return (series < benford_values).sum() == series.size
