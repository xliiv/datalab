# Build a Docker Image

Usual stuff, like

```shell
docker build . -t datalab
```

# Run a Docker Container

Usual stuff, like

```shell
docker run --net=host --name my-datalab datalab \
    bash -c "cd datalab && flask db upgrade && flask run --host=0.0.0.0"
```


# Future Work

1. Improve the project layout by using e.g [flaskr](https://github.com/pallets/flask/tree/master/examples/tutorial/flaskr)
2. Allow to specify dataset file type while adding a data set (now it's only tab-delimeted file)
3. Optimize Dockerfile size with multi-stage build
4. Use Gunicorn
5. Write tests for the tools.py module
6. Write tests for the dataset_benford view
7. Insepct why flask exits sometimes during the dataset_benford view
